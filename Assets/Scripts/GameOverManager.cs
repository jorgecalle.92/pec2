﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public Text PlayerScore;
    public Text TextLife;
    public GameObject TextGameOver;
    public Sound GameOverSound;
    public Sound TryAgainSound;

    private GameSceneManager sceneManager;

    private void Awake()
    {
        sceneManager = new GameSceneManager();
    }

    void Start()
    {
        PlayerScore.text = "MARIO\n" + PlayerPrefs.GetInt("MarioScore");
        TextLife.text = "x " +PlayerPrefs.GetInt("LifePlayer");

        if (PlayerPrefs.GetInt("LifePlayer") == 0)
        {
            if (PlayerPrefs.GetInt("MarioScore") > PlayerPrefs.GetInt("BestScore"))
            {
                PlayerPrefs.SetInt("BestScore", PlayerPrefs.GetInt("MarioScore"));
            }

            TextGameOver.SetActive(true);
            AudioManager.Instance.PlaySound(GameOverSound);

            Invoke("LoadMainMenu", 5);
        }
        else
        {
            Invoke("LoadLevel", 2);
            AudioManager.Instance.PlaySound(TryAgainSound);
        }
    }

    private void LoadMainMenu()
    {
        sceneManager.LoadScene("MainMenu");
    }

    private void LoadLevel()
    {
        sceneManager.LoadScene("Level");
    }
}