﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float moveSpeed = 1;

    private Rigidbody2D rb;
    private Vector3 movement;
    private bool startMove= false;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        //Se mueve en una única dirección, y cuando choca con algo, cambia su dirección y rotación.
        movement = new Vector3(-1, 0f, 0f);

        //Justo un segundo antes de que la cámara enfoque al enemigo,inicia su movimiento
        if(rb.transform.position.x - GameObject.FindGameObjectWithTag("MainCamera").transform.position.x  < 10)
        {
            startMove = true;
        }

        if(startMove && PlayerPrefs.GetInt("PlayerIsDead") == 0)
        {
            Move();
        }
    }

    private void Move()
    {
        rb.transform.position+= movement * Time.deltaTime * moveSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Enemy"))
        {
            moveSpeed *= -1;
        }
    }
}
