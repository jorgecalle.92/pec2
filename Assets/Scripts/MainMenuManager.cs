﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public Text BestScore;
    public Sound menuSound;

    private void Awake()
    {
        PlayerPrefs.SetInt("MarioScore", 000000);
        PlayerPrefs.SetInt("LifePlayer", 3);
        PlayerPrefs.SetInt("PlayerWin", 0);
    }

    void Start()
    {
        AudioManager.Instance.PlaySound(menuSound);
        BestScore.text= "BEST SCORE: "+ PlayerPrefs.GetInt("BestScore");
    }
}
