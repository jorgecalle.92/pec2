﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text PlayerScore;
    public Text TimeRemaining;
    public Sound LevelSound;
    public Sound PlayerDied;
    public Sound PlayerJump;
    public Sound PlayerWin;
    public Sound LevelSoundSpeed;

    void Start()
    {
        AudioManager.Instance.PlaySound(LevelSound);
    }
    
    void Update()
    {
        PlayerScore.text = "MARIO\n" + PlayerPrefs.GetInt("MarioScore");
        TimeRemaining.text = "TIME\n " + PlayerPrefs.GetInt("TimeGame");

        if (PlayerPrefs.GetInt("PlayerIsDead") == 1)
        {
            SoundDead();
        }

        if (PlayerPrefs.GetInt("PlayerIsJump") == 1 && PlayerPrefs.GetInt("PlayerIsGround") == 0)
        {
            SoundJump();
        }

        if(PlayerPrefs.GetInt("PlayerWin") == 1)
        {
            SoundWin();
        }

        if(PlayerPrefs.GetInt("TimeGame") < 380)
        {
            SoundSpeed();
        }
    }

    private void SoundDead()
    {
        AudioManager.Instance.StopMusic();
        AudioManager.Instance.StopMusicAlternative();

        if (!AudioManager.Instance.IsFXPlaying())
        {
            AudioManager.Instance.PlaySound(PlayerDied);
        }
    }

    private void SoundJump()
    {
        if (!AudioManager.Instance.IsFXPlaying())
        {
            AudioManager.Instance.PlaySound(PlayerJump);
        }
    }

    private void SoundWin()
    {
        AudioManager.Instance.StopMusic();
        AudioManager.Instance.StopMusicAlternative();

        if (!AudioManager.Instance.IsFXPlaying())
        {
            AudioManager.Instance.PlaySound(PlayerWin);
        }
    }

    private void SoundSpeed()
    {
        AudioManager.Instance.StopMusic();

        if (!AudioManager.Instance.IsAlternativePlaying())
        {
            AudioManager.Instance.PlaySound(LevelSoundSpeed);
        }
    }
}
