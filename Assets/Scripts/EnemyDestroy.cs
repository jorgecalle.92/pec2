﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroy : MonoBehaviour
{
    public Sound EnemyKick;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player") && PlayerPrefs.GetInt("PlayerIsDead") == 0)
        {
            Destroy(transform.parent.gameObject);
            PlayerPrefs.SetInt("MarioScore", PlayerPrefs.GetInt("MarioScore")+100);
            AudioManager.Instance.PlaySound(EnemyKick);
        }
    }
}
