﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 7;
    public float jumpPower = 22;
    public Transform BottomMario;
    public LayerMask groundMask;
    public GameObject TextWin;

    private Rigidbody2D rb;
    private float playerRadius= 0.07f;
    private bool onGround= true;
    private bool isRight = true;
    private GameSceneManager sceneManager;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        PlayerPrefs.SetInt("PlayerIsJump", 0);
        PlayerPrefs.SetInt("PlayerIsGround", 1);
        PlayerPrefs.SetInt("PlayerIsDead", 0);
        PlayerPrefs.SetInt("TimeGame", 400);
        sceneManager = new GameSceneManager();
    }

    private void Start()
    {
        InvokeRepeating("SusbstractTime", 1f, 1f);
    }

    void FixedUpdate()
    {
        onGround = Physics2D.OverlapCircle(BottomMario.position, playerRadius, groundMask);

        if(PlayerPrefs.GetInt("PlayerIsDead") == 0)
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                if (isRight)
                {
                    isRight = false;
                    rb.transform.Rotate(0, 180, 0);
                }

                Move();
            }

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                if (!isRight)
                {
                    isRight = true;
                    rb.transform.Rotate(0, 180, 0);
                }

                Move();
            }

            if (Input.GetKey(KeyCode.Space) && onGround)
            {
                PlayerPrefs.SetInt("PlayerIsGround", 0);
                Jump();
            }
        }
        

        if(onGround)
        {
            PlayerPrefs.SetInt("PlayerIsGround", 1);
        }
        else
        {
           PlayerPrefs.SetInt("PlayerIsGround", 0);
        }

        PlayerPrefs.SetFloat("PlayerVelocity", Input.GetAxis("Horizontal"));

        if(PlayerPrefs.GetInt("PlayerIsDead") == 1)
        {
            Invoke("PlayerDead", 4);
        }
    }

    private void Move()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        rb.transform.position += movement * Time.deltaTime * moveSpeed;
    }

    private void Jump()
    {
        PlayerPrefs.SetInt("PlayerIsJump", 1);
        rb.velocity = new Vector2(rb.velocity.x, jumpPower);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || PlayerPrefs.GetInt("TimeGame") < 1)
        {
            PlayerPrefs.SetInt("PlayerIsDead", 1);
            Jump();
            
            rb.gameObject.GetComponent<BoxCollider2D>().isTrigger= true;
        }

        if(collision.gameObject.CompareTag("Flag"))
        {
            PlayerPrefs.SetInt("PlayerWin", 1);
            TextWin.SetActive(true);
            Invoke("LoadMainMenu", 8);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("DeathZone"))
        {
            PlayerPrefs.SetInt("PlayerIsDead", 1);
        }
    }

    private void SusbstractTime()
    {
        if(PlayerPrefs.GetInt("PlayerIsDead") == 0)
        {
            PlayerPrefs.SetInt("TimeGame", PlayerPrefs.GetInt("TimeGame")-1); ;
        }
    }

    private void PlayerDead()
    {
        PlayerPrefs.SetInt("LifePlayer", PlayerPrefs.GetInt("LifePlayer") - 1);
        sceneManager.LoadScene("GameOver");
    }

    private void LoadMainMenu()
    {
        sceneManager.LoadScene("MainMenu");
    }
}