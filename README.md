# PEC2

En este proyecto se ha recreado el primer nivel del videojuego Super Mario Bros.
Este juego se compone de tres escenas, el menú principal, el nivel y la pantalla de Game Over.

*  La escena de **MainMenu** se compone de dos botones, uno para comenzar la partida y otro para salir del juego. Además, se muestra la máxima puntuación que ha logrado conseguir el jugador. Esta escena contiene los siguientes componentes:

    **1. AudioManager:** mediante los scripts Sound y AudioManager se realiza un control de toda la música y los efectos de sonido del juego, por lo que se guarda como un Prefab y se reutiliza en las otras dos escenas.
    
    **2. GameSceneManager:** el script GameSceneManager es el que se encarga de controlar los cambios entre escenas, mediante una función llamada LoadScene a la cual únicamente hay que pasarle como parámetro la escena que se quiere cargar. En este caso, el evento onClick del botón Play será el que realice el cambio de escena al Level.
    
    **3. MainMenuManager:** el script MainMenuManager, además de inicializar la música de esta escena, también se encarga de añadir el texto con la puntuacón máxima obteniéndolo con PlayerPrefbs. También, mediante el PlayerPrefbs, se inicializan los valores de la puntuación actual, la  vida inicial y un booleano para comprobar si el jugador ha ganado o no la partida.
    
*  En cuanto a la escena de **Level**, habrá una interfaz en la que se muestra la puntuación actual del jugador, el nivel en el que sencuentra, aunque en este proyecto solo se ha replicado el primer nivel del juego, y el tiempo restante que le queda al jugador para completar el nivel. El jugador tendrá que sortear varios peligros a lo largo del nivel, unos Goombas y un Koopa, además de que puede caerse en distintos puntos del nivel. Para poder evitar estos peligros, el personaje además de moverse puede saltar, pudienndo además aplastar a los enemigos saltando sobre ellos. En cambio, si uno de los enemigos toca a Mario, el personaje perderá una vida. Para poder completar el nivel, el jugador deberá tocar la bandera que se encuentra a la izquierda del castillo al final del nivel. En ese momento aparecerá un texto informando al jugador que ha ganado, además de sonar la música del nivel completado:

    **1. AudioManager:** tiene el mismo comportamiento que en la escena del menú principal.
    
    **2. GameSceneManager:** tiene el mismo comportamiento que en la escena del menú principal.
    
    **3. GameManager:** en el script GameManager se inicializa la música principal del nivel, además de encargarse de poner los textos con la puntuación actual y el tiempo restante. En este script también se gestionan los distintos efectos de sonido como el salto de Mario o cuando aplasta a un enemigo y la diferente música del juego, como la música principal rápida en cuanto ha pasado cierto tiempo (en este caso, como el primer nivel es demasiado corto y fácil, la música empezará a sonar cuando el tiempo esté por debajo de los 380 segundos).
    
    **4. Mario:** para el control del personaje del jugador, se hace uso de dos scripts. Uno de ellos es el AnimationPlayer, que se encargará de gestionar las diferentes animaciones del personaje en función de sus diferentes estados, como si se encuentra saltando que sube el puño o si le ha dañado un enemigo, que pegará un salto poniendo una cara asustada y mirando hacia la pantalla. El otro script que contiene el personaje es el del PlayerController, que se encargará de controlar el movimiento del jugador mediante las teclas de direcciones o A para moverse a la izquierda y D para la derecha, además de la tecla Espacio para realizar los saltos. En este script también se controla si es dañado por el enemigo, cae por un precipicio o llega al final del nivel y gana la partida.
    
    **5. Main Camera:** al tratarse de un juego horizontal en 2D, la cámara se desplazará únicamente de forma horizontal siguiendo en todo momento al jugador. Para ello, el script PlayerFollower se encarga de hallar la posición actual del jugador para que la cámara le vaya siguiendo.
    
    **6. Goomba y Koopa:** estos dos enemigos son controlados por dos scripts llamados EnemyController y EnemyDestroy. El primero es que se encarga de mover al enemigo, que se moverá de forma inicial a la izquierda y en el momento que choque con algún obstáculo, cambiará su dirección. Cada uno de los enemigos solo comenzará a moverse cuando la cámara se encuentre justo a la izquierda de su posición. A partir de ese momento no cesará su movimiento aunque el jugador ya se encuentre alejado del enemigo. En cambio, el script EnemyDestroy es el que se encarga de detectar si Mario golpea al enemigo en su cabeza, en cuyo momento sumará 100 puntos al jugador y el enemigo será destruido.
    
*  La escena de **GameOver** aparecerá cada vez que el jugador pierda una vida, mostrando por pantalla las vidas que le quedan al jugador. En caso de que ya no le quede ninguna vida, sonará la música de Game Over y se cambiará la escena del menú principal:

    **1. AudioManager:** tiene el mismo comportamiento que en la escena del menú principal.
    
    **2. GameSceneManager:** tiene el mismo comportamiento que en la escena del menú principal.
    
    **3. GameOverManager:** el script GameOverManager detectará las vidas que le quedan al jugador, para de esta forma regresar a la escena del nivel o ir a la escena del menú principal en el caso de que el jugador haya perdido todas las vidas. 